package br.ufc.quixada.model;
public class EntradaDeCinema {

	private String tituloDeFilme;
	private int horario;
	private int sala;
	private int poltrona;
	private float valorOriginal;
	private boolean disponivel;
	private int nascimento;
	private int carteira;
	
	
	public EntradaDeCinema(String tituloDeFilme, int horario, int sala, int poltrona, int valorOriginal) {
		this.tituloDeFilme = tituloDeFilme;
		this.horario = horario;
		this.sala = sala;
		this.poltrona = poltrona;
		this.valorOriginal = valorOriginal;
		
		this.nascimento = 0;
		this.carteira = 0;
		this.disponivel = true;
	}
	
	/*GET PEGA, SET ALTERA*/
	public String getTituloDeFilme(String tituloDeFilme) {
		return this.tituloDeFilme;
	}
	
	public int getHorario(int horario) {
		return this.horario;
	}
	
	public int getSala(int sala) {
		return this.sala;
	}
	
	
	public int getPoltrona(int poltrona) {
		return this.poltrona;
	}
	
	public float getValorOriginal(float valorOriginal) {
		return this.valorOriginal;
	}
	
	/*PEGANDO A IDADE E A CALCULANDO COM A DATA ATUAL*/
	
	 
	 public int getNascimento() {
		return this.nascimento;
	}
	public int getCarteira() {
		return this.carteira;
	}


	public float calculaValorComDesconto(int anoNascimento, int carteira) {
		 int idade = 2018 - anoNascimento;
		 if(idade<12) {
			return valorOriginal - (valorOriginal*50)/100;
		 }else if((idade>=12) && (idade<=15)){
			 return valorOriginal - (valorOriginal*40)/100;
		 }else if((idade>=16) && (idade<=20)){
			 return valorOriginal - (valorOriginal*30)/100;
		 }else{
			 return valorOriginal - (valorOriginal*20)/100;
		 }
		 
	 }
	 
	public boolean realizarVenda(int anoNascimento, int carteira) {
		 if(disponivel == true) {
			 this.disponivel = false;
			 this.nascimento = anoNascimento;
			 this.carteira = carteira;
			 
			 return true;
			 
		 }
		 
		return false;
	 }
	 
	 public void setDisponivel(boolean disponivel) {
		 if(disponivel==false) {
			 System.out.println("Compra realizada com sucesso!");
		 }else {
			 System.out.println("Erro ao realizar compra!");
		 }
	 }
	 
	 public String toString() {
			String ingresso = "";
			ingresso = "Filme: " + this.tituloDeFilme+'\n'+
					"Horario: " +this.horario+'\n'+
					"Sala: " +this.sala+'\n'+
					"Poltrona: " +this.poltrona+'\n'+
					"Valor original: "+this.valorOriginal+'\n'+
					"Valor do ingresso: "+calculaValorComDesconto(nascimento, carteira)+'\n'+
					this.disponivel;
					
			return ingresso;
		}




}