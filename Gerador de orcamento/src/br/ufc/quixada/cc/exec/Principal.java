package br.ufc.quixada.cc.exec;
import br.ufc.quixada.cc.model.*;
import java.util.Scanner;

public class Principal {

	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		boolean adminmode = false;
		
		Menu menu = new Menu();
		
		System.out.println("Usu�rio: ");
		String user = scan.nextLine();
		
		System.out.println("Senha: ");
		String senha = scan.nextLine();
		
		if(user.equalsIgnoreCase("admin") && senha.equalsIgnoreCase("admin")) {
			adminmode = true;
		}
		
		System.out.println("\nBem vindo, " + user +".\n\n");
		menu.menu(adminmode);
		
	}
}