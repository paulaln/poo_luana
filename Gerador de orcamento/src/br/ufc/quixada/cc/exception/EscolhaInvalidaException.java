package br.ufc.quixada.cc.exception;

public class EscolhaInvalidaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EscolhaInvalidaException(String arg0) {
		super(arg0);
	}

	
}
