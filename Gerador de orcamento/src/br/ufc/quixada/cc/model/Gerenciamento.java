package br.ufc.quixada.cc.model;

import br.ufc.quixada.cc.interfaces.Imprimivel;

import java.util.*;


public class Gerenciamento implements Imprimivel{
	
	
	Map<String, Empresa> trabalhos = new HashMap<String, Empresa>();
	Map<String, Empresa> namemap = new HashMap<String, Empresa>();
	Map<String, Cliente> clientes = new HashMap<String, Cliente>();
	Map<Integer, Orcamento> orcamento = new HashMap<Integer, Orcamento>();
	Set<String> keys = trabalhos.keySet();
	Set<String> keys2 = namemap.keySet();
	Set<String> keys4 = clientes.keySet();
	Set<Integer> keys5 = orcamento.keySet();
	
	List<Orcamento> orcamentos = new ArrayList<Orcamento>();

	
	
	//-----------------------------------------LISTA DE TRABALHOS------------------------------------------------------
	
	public void Adicionar(Empresa trab) {
		if(trabalhos.containsKey(trab.getId())) {
			System.out.println("Trabalho j� existente.\n");
		}
		
		else {
		trabalhos.put(trab.getId(), trab);
		namemap.put(trab.getNome(), trab);
		System.out.println("\nAdicionado com sucesso!\n");
		}
	}
	
	
	public void Remover(String id) {
		
		if (trabalhos.isEmpty()) {
			System.out.println("N�o h� trabalhos a remover.\n");
		}
		
		else {
		
			if(trabalhos.containsKey(id)) {
				Empresa emp2 = trabalhos.get(id);
				String emp2name = emp2.getNome();
				
				trabalhos.remove(id);
				namemap.remove(emp2name);
				
				System.out.println("Remo��o conclu�da!\n");
			}
		
			else {
				System.out.println("Trabalho inexistente.\n");
			}
		}
	}
	
	
	
	public void Pesquisar(String id) {
		
		if (trabalhos.isEmpty()) {
			System.out.println("N�o h� servi�os dispon�veis.\n");
		}
		
		else {
			if(trabalhos.containsKey(id)) {
				System.out.println(trabalhos.get(id));
			}
			
			else {
				System.out.println("Trabalho n�o encontrado.\n");
			}
		
		
		}
		
	}
		
	
	public void PesquisarNome(String nome) {
		
		if (namemap.isEmpty()) {
			System.out.println("N�o h� servi�os dispon�veis.\n");
		}
		
		else {
			if(namemap.containsKey(nome)) {
				System.out.println(namemap.get(nome));
			}
			
			else {
				System.out.println("Trabalho n�o encontrado.\n");
			}
		
		
		}
		
	}
	
		
	public void MostrarTudo(){
		
		if (trabalhos.isEmpty()) {
			System.out.println("N�o h� servi�os dispon�veis.\n");
		}
		
		else {

			for(String id : keys) {
				System.out.println(trabalhos.get(id));
			}
		}
		
	}
	
	public Empresa ModificarTrabalho(String id) {
		if (trabalhos.containsKey(id)) {
			Empresa aux = trabalhos.get(id);
			return aux;
		}
		
		else {
			System.out.println("N�o ha trabalho.");
		}
		return null;
	}
	
	
	
	//---------------------------LISTA DOS CLIENTES (OR�AMENTO) ------------------------------------------
	
	public void AdicionarDaLista(Cliente cliente, String id) {
		Empresa empr = trabalhos.get(id);
		
		if(trabalhos.isEmpty()) {
			System.out.println("N�o h� trabalhos dispon�veis.");
		}
		
		else {
		
			if(cliente.listaDeCompras.containsKey(empr.getId())) {
				System.out.println("Trabalho j� est� na lista.\n");
			}
		
			else {
				Empresa aux = trabalhos.get(id);
				cliente.listaDeCompras.put(id, aux);
				}
		
		}
	}

	
	public void RemoverDaLista(Cliente cliente, String nome) {
		
		if (cliente.listaDeCompras.isEmpty()) {
			System.out.println("N�o h� trabalhos a remover.\n");
		}
		
		else {
		
			if(cliente.listaDeCompras.containsKey(nome)) {
				cliente.listaDeCompras.remove(nome);				
				System.out.println("Remo��o conclu�da!\n");
			}
		
			else {
				System.out.println("Trabalho inexistente.\n");
			}
		}
	}
	
	
	@Override
	public void mostrarLista(Cliente cliente) {
		
		
		if (cliente.listaDeCompras.isEmpty()) {
			System.out.println("N�o h� trabalhos na lista.\n");
		}
		
		else {
			
			Set<String> keyaux = cliente.getKeys();
			
			for(String lista : keyaux) {
				System.out.println(cliente.listaDeCompras.get(lista));
			}
		}
	}
		
	public double ListaTotal(Cliente cliente) {
		double total = 0;
		
		
		Set<String> keyaux = cliente.getKeys();
		
		for(String list : keyaux) {
			Empresa aux = cliente.listaDeCompras.get(list);
			total = total + aux.getValorUnitario();
		}	
		return total;
	}
	
	//--------------------------------------- LISTA DE CLIENTES ----------------------------------------

	public void ClienteAdd(Cliente cliente) {
		if(clientes.containsKey(cliente.getCpf())) {
			System.out.println("Cliente ja cadastrado.");
		}
		
		else {
			clientes.put(cliente.getCpf(), cliente);
			System.out.println("Cadastrado com sucesso.\n");
		}
	}
	
	public void ClienteRemove(String cpf){
		if(clientes.containsKey(cpf)) {
			clientes.remove(cpf);
			System.out.println("Remocao sucedida.");
		}
		
		else {
			System.out.println("Cliente inexistente.");
		}
	}
	
	public Cliente pesquisarCliente(String cpf) {
		
		if (clientes.isEmpty()) {
			System.out.println("N�o h� clientes dispon�veis.\n");
		}
		
		else {
			if(clientes.containsKey(cpf)) {
				Cliente c1 = clientes.get(cpf); 
				return c1;
			}else {
				System.out.println("Cliente n�o encontrado.\n");
			}
		
		}
		return null;
		
	}
	
	public void MostrarClientes() {
		if(clientes.isEmpty()) {
			System.out.println("Nao ha clientes.");
		}
		
		else {
			for(String cpf : keys4) {
				System.out.println(clientes.get(cpf));
			}
		}
	}
	
	
	public void clear() {
		for (int i = 0; i < 50; ++i) System.out.println();
	}


	public boolean ListaVazia() {
		if (clientes.isEmpty()) {
			return true;
		}
		
		else {
			return false;
		}
		
	}
	
	public void adicionarOrcamento(Orcamento orcamento) {
		orcamentos.add(orcamento);
	}
	
	public int tamanhoListaOrcamento() {
		return orcamentos.size();
	}
	
}
