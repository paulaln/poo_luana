package br.ufc.quixada.cc.model;

import java.util.*;

import br.ufc.quixada.cc.exception.*;


public class Menu {

	static Scanner scan = new Scanner(System.in);
	Gerenciamento ger = new Gerenciamento();
	Cliente c1 = null;

	public void menu(boolean admmode) {

		try {	

			System.out.println("_________________________________________________");
			System.out.println("                                                 ");
			System.out.println("  >  >  >  GERENCIAMENTO DE OR�AMENTOS  <  <  <   ");
			System.out.println("_________________________________________________\n");

			System.out.println("___________________________________________\n");
			System.out.println("[1] - Or�amentos\n");
			System.out.println("[2] - Lista de trabalhos dispon�veis\n");
			System.out.println("[3] - Pesquisar trabalhos\n");
			System.out.println("-------------------------------------------\n");
			System.out.println("[4] - MODO DE ADMIN\n");
			System.out.println("-------------------------------------------\n");
			System.out.println("[5] - Finalizar operacao");
			System.out.println("___________________________________________\n");
			System.out.println("--> ");

			String var = scan.nextLine();

			switch(var) {

			case "1":
				ger.clear();
				clientmode();
				menu(admmode);
				break;

			case "2":
				ger.clear();
				ger.MostrarTudo();
				menu(admmode);
				break;

			case "3":
				ger.clear();
				searchmode();
				
				menu(admmode);
				break;
			case "4":
				if(admmode) {
					ger.clear();
					admmenu();
				}

				else {
					ger.clear();
					System.out.println("N�o � admin! YOU SHALL NOT PASS!\n");
					menu(admmode);
				}
				break;

			case "5":
				ger.clear();
				System.out.println("Adeus, jovem padawan!");
				break;

			default:
				ger.clear();
				throw new EscolhaInvalidaException("Escolha invalida.\n");
			}

		} 	catch (Exception e) {
			System.out.println(e.getMessage());
			menu(admmode);
		}

	}

	public void admmenu() {

		try {

			System.out.println("__________________________________________");
			System.out.println("                                          ");
			System.out.println("  >  >  > MODO DE ADMINISTRA��O  <  <  <   ");
			System.out.println("__________________________________________\n");

			System.out.println("\n--> Voc� est� no modo de administrador.\n");

			System.out.println("_____________________________________\n");
			System.out.println("[1] - ADICIONAR TRABALHOS\n");
			System.out.println("[2] - REMOVER TRABALHOS\n");
			System.out.println("[3] - ATUALIZAR PRE�O DOS TRABALHOS\n");
			System.out.println("[4] - LISTA DE TRABALHOS\n");
			System.out.println("-------------------------------------\n");
			System.out.println("[5] - ADICIONAR CLIENTE\n");
			System.out.println("[6] - REMOVER CLIENTE\n");
			System.out.println("[7] - LISTA DE CLIENTES\n");
			System.out.println("-------------------------------------\n");
			System.out.println("[8] - Voltar ao modo normal\n");
			System.out.println("_____________________________________\n");
			System.out.println("--> ");

			String admop = scan.nextLine();

			switch(admop) {

			case "1":
				ger.clear();
				System.out.println("Criar: \n\n");
				System.out.println("[1] - Servi�o\n");
				System.out.println("[2] - Produto\n");

				String opp = scan.nextLine();

				if(opp.equals("1")) {
					Empresa_Servico serv = new Empresa_Servico();
					serv.Dados();
					ger.Adicionar(serv);
					admmenu();
				}

				else if(opp.equals("2")) {
					Empresa_Produto prod = new Empresa_Produto();
					prod.Dados();
					ger.Adicionar(prod);
					admmenu();
				}

				else {
					throw new EscolhaInvalidaException("Escolha invalida.\n");
				}

				break;

			case "2":
				ger.clear();
				ger.MostrarTudo();
				System.out.println("Remover trabalho de ID: ");
				String rmv = scan.nextLine();

				ger.Remover(rmv);
				admmenu();

				break;

			case "3":
				ger.clear();
				ger.MostrarTudo();
				System.out.println("Qual trabalho deseja modificar? (Escolha por ID)");
				String mod = scan.nextLine();
				Empresa aux = ger.ModificarTrabalho(mod);

				if(aux != null) {
					System.out.println("Qual o novo pre�o do trabalho " +aux.getNome() + "?");
					aux.setValorUnitario(Double.parseDouble(scan.nextLine()));
				}

				else {
					System.out.println("Trabalho nao identificado.");
				}

			case "4":
				ger.clear();
				ger.MostrarTudo();
				admmenu();
				break;


			case "5":
				Cliente cliente = new Cliente();
				cliente.Dados();
				ger.ClienteAdd(cliente);


				Orcamento orcamento = new Orcamento(ger.tamanhoListaOrcamento() + 1, cliente, ger.ListaTotal(cliente));
				ger.adicionarOrcamento(orcamento);
				admmenu();
				break;

			case "6":
				System.out.println("Remover cliente com CPF: ");
				String clientermv = scan.nextLine();
				ger.ClienteRemove(clientermv);
				admmenu();
				break;

			case "7":
				ger.clear();
				ger.MostrarClientes();
				admmenu();
				break; 

			case "8":
				ger.clear();
				menu(true);
				break;

			default:
				ger.clear();
				throw new EscolhaInvalidaException("Escolha invalida.\n");

			}

		} 	catch (Exception e) {
			System.out.println(e.getMessage());
			admmenu();
		}
	}

	public void searchmode() {
		try {
			System.out.println("Pesquisar trabalho por:\n");
			System.out.println("[1] - ID\n");
			System.out.println("[2] - NOME\n");
			System.out.println("--> ");
			String sla = scan.nextLine();

			if(sla.equals("1")) {
				System.out.println("Pesquisar pelo ID: ");	
				String idsrch = scan.nextLine();
				ger.Pesquisar(idsrch);

				System.out.println("Fazer outra pesquisa?\n\n [S]||[N]");
				String slc = scan.nextLine();

				if(slc.equalsIgnoreCase("s")) {
					searchmode();
				}
			}

			else if(sla.equals("2")) {
				System.out.println("Pesquisar trabalho de nome: ");
				String nome = scan.nextLine();
				ger.PesquisarNome(nome);
				
				System.out.println("Fazer outra pesquisa?\n\n [S]||[N]");
				String slc = scan.nextLine();

				if(slc.equalsIgnoreCase("s")) {
					searchmode();
				}
			}

			else {
				throw new EscolhaInvalidaException("Escolha invalida.");
			}
		}	catch(Exception e) {
			System.out.println(e.getMessage());
			searchmode();
		}
	}


	public void clientmode() {

		if (ger.ListaVazia()) {
			System.out.println("Lista vazia. Adicione clientes.");
			menu(true);
		}else {

			
			while(c1 == null) {
				ger.MostrarClientes();
				System.out.print("Escolha o cpf do cliente que deseja realizar compras: ");
				String cpf = scan.nextLine();
				c1 = ger.pesquisarCliente(cpf);

			}



			try {
				System.out.println(" __________________________________________");
				System.out.println("                                           ");
				System.out.println("  >  >  > OR�AMENTOS  <  <  <  ");
				System.out.println("___________________________________________\n");
				System.out.println(" ___________________________________________________\n");
				System.out.println("[1] - Adicionar/Remover trabalhos da minha lista\n");
				System.out.println("[2] - Lista de trabalhos dispon�veis            \n");
				System.out.println("[3] - Pesquisar trabalhos                       \n");
				System.out.println("[4] - Minha lista/Or�amento                     \n");
				System.out.println("[5] - Encerrar                                  ");
				System.out.println("____________________________________________________");

				String cltop = scan.nextLine();

				switch(cltop) {

				case "1":
					System.out.println("Deseja: ");
					System.out.println("[1] - Adicionar\n");
					System.out.println("[2] - Remover\n");
					System.out.println("--> ");
					String lol = scan.nextLine();

					if(lol.equals("1")) {
						ger.clear();
						ger.MostrarTudo();
						System.out.println("Trabalhos s�o adicionados da lista pelo ID.");
						System.out.println("Adicionar trabalho de ID: ");
						String add = scan.nextLine();

						ger.AdicionarDaLista(c1, add);
						clientmode();
					}

					else if(lol.equals("2")) {
						ger.clear();
						ger.mostrarLista(c1);
						System.out.println("Remover trabalho de nome: .");
						String rem = scan.nextLine();

						ger.RemoverDaLista(c1,rem);
						clientmode();
					}

					else {
						throw new EscolhaInvalidaException("Escolha invalida.");
					}
					break;

				case "2":
					ger.clear();
					ger.MostrarTudo();
					clientmode();


				case "3":
					ger.clear();
					searchmode();
					clientmode();
					break;

				case "4":
					ger.clear();
					ger.mostrarLista(c1);
					System.out.println(ger.ListaTotal(c1));

					clientmode();
					break;

				case "5":
					ger.clear();
					System.out.println("Obrigado pela prefer�ncia!");
					c1 = null;
					
					break;

				default:
					throw new EscolhaInvalidaException("Escolha invalida.\n");
				}
			}	catch (Exception e) {
				System.out.println(e.getMessage());
				clientmode();
			}
		}
	}


}