package br.ufc.quixada.cc.exec;
import br.ufc.quixada.cc.model.*;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Pessoa p = new Pessoa("Paula", "Oscar Barbosa", "00000");
		Fornecedor f = new Fornecedor("Paula", "Oscar Barbosa", "00000", 15363.5, 2637.8);
		Empregado e = new Empregado("Paula", "Oscar Barbosa", "00000", 6, 644.8, 6484.1);
		Administrador adm = new Administrador("Paula", "Oscar Barbosa", "00000", 6, 644.8, 6484.1,132.4);
		Operario op = new Operario("Paula", "Oscar Barbosa", "00000", 6, 644.8, 6484.1, 12.4, 12.5);
		Vendedor vend = new Vendedor("Paula", "Oscar Barbosa", "00000", 6, 644.8, 6484.1, 45.2, 56.1);
		
		p.mostrarPessoa();
		f.mostrarFornecedor();
		e.mostrarEmpregado();
		adm.mostrarAdmin();
		op.mostrarOperario();
		vend.mostrarVendedor();
	}

}
