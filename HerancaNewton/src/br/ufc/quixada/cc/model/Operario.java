package br.ufc.quixada.cc.model;

public class Operario extends Empregado{
	
	private double valorProducao;
	private double comissao;
	
	
	public Operario() {
		super();
	}
	


	public Operario(String nome, String endereco, String telefone, int codSetor, double salarioBase, double imposto, double valorProducao, double comissao) {
		super(nome, endereco, telefone, codSetor, salarioBase, imposto);
		this.valorProducao = valorProducao;
		this.comissao = comissao;
	}




	public double getValorProducao() {
		return valorProducao;
	}


	public void setValorProducao(double valorProducao) {
		this.valorProducao = valorProducao;
	}


	public double getComissao() {
		return comissao;
	}


	public void setComissao(double comissao) {
		this.comissao = comissao;
	}
	
	@Override
	public double calcularSalario() {
		double salarioOp = super.calcularSalario() + this.comissao;
		return salarioOp;
	}
	
	public void mostrarOperario() {
		System.out.println("Dados do operario");
		super.mostrarEmpregado();
		System.out.println("Valor da producao: " + this.valorProducao);
		System.out.println("Comissao: " + this.comissao);
		System.out.println(calcularSalario());
	}
	

}
