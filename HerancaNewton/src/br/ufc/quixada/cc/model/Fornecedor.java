package br.ufc.quixada.cc.model;

public class Fornecedor extends Pessoa{
	
	private double valorCredito;
	private double valorDivida;
	
	public Fornecedor() {
		super();
	}

	public Fornecedor(String nome, String endereco, String telefone, double valorCredito, double valorDivida) {
		super(nome, endereco, telefone);
		this.valorCredito = valorCredito;
		this.valorDivida = valorDivida;
	}

	public double getValorCredito() {
		return valorCredito;
	}

	public void setValorCredito(double valorCredito) {
		this.valorCredito = valorCredito;
	}

	public double getValorDivida() {
		return valorDivida;
	}

	public void setValorDivida(double valorDivida) {
		this.valorDivida = valorDivida;
	}
	
	public double obterSaldo(double valorCredito, double valorDivida) {
		double diferenca = valorCredito - valorDivida;
		return diferenca;
	}
	
	public void mostrarFornecedor() {
		System.out.println("Dados do fornecedor: ");
		super.mostrarPessoa();
		System.out.println("Valor do credito: " + this.valorCredito);
		System.out.println("Valor da divida: " + this.valorDivida);
		
	}
	
	
	

}
