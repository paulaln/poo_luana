package br.ufc.quixada.cc.model;

public class Administrador extends Empregado{
	
	private double ajudaDeCusto;

	public Administrador() {
		super();
	}

	public Administrador(String nome, String endereco, String telefone, int codSetor, double salarioBase, double imposto, double ajudaDeCusto) {
		super(nome, endereco, telefone, codSetor, salarioBase, imposto);
		this.ajudaDeCusto = ajudaDeCusto;
	}

	public double getAjudaDeCusto() {
		return ajudaDeCusto;
	}

	public void setAjudaDeCusto(double ajudaDeCusto) {
		this.ajudaDeCusto = ajudaDeCusto;
	}
	
	@Override
	public double calcularSalario() {
		double salarioAdm = super.calcularSalario() + this.ajudaDeCusto;
		return salarioAdm;
	}
	
	public void mostrarAdmin() {
		System.out.println("Dados do administrador");
		super.mostrarEmpregado();
		System.out.println("Ajuda de custo: " + this.ajudaDeCusto);
		System.out.println(calcularSalario());
	}
}
