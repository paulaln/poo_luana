package br.ufc.quixada.cc.model;

public class Vendedor extends Empregado{
	
	private double valorVendas;
	private double comissaoP;
	
	
	public Vendedor() {
		super();
	}


	public Vendedor(String nome, String endereco, String telefone, int codSetor, double salarioBase, double imposto, double valorVendas, double comissao) {
		super(nome, endereco, telefone, codSetor, salarioBase, imposto);
		this.valorVendas = valorVendas;
		this.comissaoP = (valorVendas*100)/salarioBase;
	}


	public double getValorVendas() {
		return valorVendas;
	}


	public void setValorVendas(double valorVendas) {
		this.valorVendas = valorVendas;
	}


	public double getComissao() {
		return comissaoP;
	}


	public void setComissao(double comissao) {
		this.comissaoP = comissao;
	}
	
	@Override
	public double calcularSalario(){
		double comissaoEmDinheiro = (super.getSalarioBase() + this.comissaoP)/100;
		double salarioDoVendedor = super.calcularSalario() + comissaoEmDinheiro;
		
		return salarioDoVendedor;
		
	}
	
	public void mostrarVendedor() {
		System.out.println("Dados do vendendor");
		super.mostrarEmpregado();
		System.out.println("Valor de vendas: " + this.valorVendas);
		System.out.println("Comissao em porcentagem: " + this.comissaoP);
		System.out.println(calcularSalario());
	}
	
	
	

}
