package br.ufc.quixada.cc.model;

public class Empregado extends Pessoa{
	
	private int codSetor;
	private double salarioBase;
	private double imposto;
	
	public Empregado() {
		super();
	}

	public Empregado(String nome, String endereco, String telefone, int codSetor, double salarioBase, double imposto) {
		super(nome, endereco, telefone);
		this.codSetor = codSetor;
		this.salarioBase = salarioBase;
		this.imposto = imposto;
	}

	public int getCodSetor() {
		return codSetor;
	}

	public void setCodSetor(int codSetor) {
		this.codSetor = codSetor;
	}

	public double getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}

	public double getImposto() {
		return imposto;
	}

	public void setImposto(double imposto) {
		this.imposto = imposto;
	}
	
	public double calcularSalario() {
		double salario = this.salarioBase - this.imposto;
		return salario;
	}
	
	public void mostrarEmpregado() {
		System.out.println("Dados do empregado");
		super.mostrarPessoa();
		System.out.println("Codigo do setor: " + this.codSetor);
		System.out.println("Salario base: " + this.salarioBase);
		System.out.println("Imposto: " + this.imposto);
		
	}
	
	
	

}
