package br.ufc.quixada.cc.exec;

import java.util.*;
import br.ufc.quixada.cc.model.*;

public class Executavel {
	public static Scanner in = new Scanner(System.in);
	public static void main(String[] args) {
		
		ContaBancaria paula  = new ContaCorrente(1234, 233.5, 5);
		ContaBancaria joel  = new ContaCorrente(6484, 3748, 10);
		ContaBancaria rita  = new ContaPoupanca(4647, 4702, 500);
		ContaBancaria luana  = new ContaPoupanca(421631, 800, 100);
		Relatorio r1 = new Relatorio();
		Banco b1 = new Banco();
		
		b1.inserir(paula);
		b1.inserir(joel);
		b1.inserir(rita);
		b1.inserir(luana);
		
		
		int numConta, op1 = 0, op2 = 0;
		double saldo, valor;
		
		do {
			System.out.println("       ** Menu **");
			System.out.println("---------------------------");
			System.out.println("1 - Criar conta");
			System.out.println("2 - Selecionar conta");
			System.out.println("3 - Remover conta");
			System.out.println("4 - Gerar relatorio");
			System.out.println("5 - Finalizar a aplicacao");
			System.out.print("Opcao: ");
			op1 = in.nextInt();
			switch(op1){
			    case 1:
				     System.out.println("Tipo de conta");
				     System.out.println("1 - Conta corrente");
				     System.out.println("2 - Conta poupanca");
				     System.out.print("Opcao: ");
				     op2 = in.nextInt();
				     switch(op2){
				     	case 1: 
				     		System.out.print("Numero da conta: ");
				     		numConta = in.nextInt();
				     		System.out.print("Saldo: ");
				     		saldo = in.nextDouble();
				    		ContaBancaria novoCorrente  = new ContaCorrente(numConta, saldo, 5);
				    		b1.inserir(novoCorrente);
				    		System.out.print("Conta criada com sucesso!");
				    		
				    		break;
				    		
				     	case 2:
				     		System.out.print("Numero da conta: ");
				     		numConta = in.nextInt();
				     		System.out.print("Saldo: ");
				     		saldo = in.nextDouble();
				    		ContaBancaria novoPoupanca  = new ContaPoupanca(numConta, saldo, 100);
				    		b1.inserir(novoPoupanca);
				    		System.out.print("Conta criada com sucesso!");
				    		break;
				    		
				    	default: System.out.println("Operacao invalida!");
	
				     		
				     }
				     
				   break;
			    case 2:
			    	System.out.println("Numero da conta: ");
			    	numConta = in.nextInt();
			    	if(b1.procurar(numConta) != null) {
			    		do{
			    		System.out.println("1 - Depositar");
			    		System.out.println("2 - Sacar");
			    		System.out.println("3 - Transferir");
			    		System.out.println("4 - Gerar relatório");
			    		System.out.println("5 - Retornar ao menu anterior");
			    		System.out.print("Opcao: ");
			    		op2 = in.nextInt();
			    		switch(op2) {
			    		case 1:
			    			System.out.print("Valor: ");
				    		valor = in.nextDouble();
			    			b1.procurar(numConta).depositar(valor);
			    			System.out.println("Valor depositado!");
			    			break;
			    			
			    		case 2:
			    			System.out.print("Valor: ");
				    		valor = in.nextDouble();
			    			if(b1.procurar(numConta).sacar(valor) != 0) {
			    				System.out.println("Valor sacado!");
			    			}else {
			    				System.out.println("Saque ultrapassa o limite disponivel!");
			    			}
			    			
			    			break;
			    			
			    		case 3:
			    			System.out.print("Valor a transferir: ");
				    		valor = in.nextDouble();
				    		System.out.println("Numero da conta que voce vai transferir:");
			    			int numConta2 = in.nextInt();
			    			if(b1.procurarConta(numConta2) == true) {
				    			b1.procurar(numConta).transferir(valor, numConta2);
				    			System.out.println("Valor transferido!");
	
			    			}else {
			    				System.out.println("Conta inexistente!");
			    			}
			    			break;
			    			
			    		case 4:
			    			r1.gerarRelatorio(b1.procurar(numConta));
			    		
			    		}
		    		}while(op2 != 5);
		    	}else {
		    		System.out.println("Conta inexistente!");
		    	}
		    	
		    	break;
		    	
		    case 3:
		    	System.out.print("Numero da conta:");
		    	numConta = in.nextInt();
		    	if(b1.procurarConta(numConta) == true) {
		    		b1.remover(b1.procurar(numConta));
		    		System.out.println("Conta removida!");
		    	}else {
		    		System.out.println("Conta inexistente!");
		    	}
		    	break;
		    case 4: 
		    	b1.mostrarDados();
		    	break;
		    case 5:
		    	System.out.println("Aplicacao finalizada!");
		    	break;
		    	
		    default: System.out.println("Opcao incorreta!");
		}
		}while(op1 != 5);
	}
}
