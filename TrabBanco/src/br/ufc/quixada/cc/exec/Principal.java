package br.ufc.quixada.cc.exec;

import br.ufc.quixada.cc.model.*;

public class Principal {


	public static void main(String[] args) {
		
		ContaBancaria paula  = new ContaCorrente(1234, 233.5, 5);
		ContaBancaria joel  = new ContaCorrente(6484, 3748, 10);
		ContaBancaria rita  = new ContaPoupanca(4647, 4702, 500);
		ContaBancaria luana  = new ContaPoupanca(421631, 800, 100);
		ContaBancaria fabiano  = new ContaPoupanca(421631, 800, 100);

		Relatorio r1 = new Relatorio();
		Banco b1 = new Banco();
		
		b1.inserir(paula);
		b1.inserir(joel);
		b1.inserir(rita);
		b1.inserir(luana);
		b1.inserir(fabiano);
		
		b1.remover(fabiano);
		
		paula.sacar(30);
		joel.sacar(1000);
		rita.sacar(4800);
		luana.sacar(910);
		
		r1.gerarRelatorio(paula);
		r1.gerarRelatorio(joel);
		r1.gerarRelatorio(rita);
		r1.gerarRelatorio(luana);
		
		
	}

	

}
