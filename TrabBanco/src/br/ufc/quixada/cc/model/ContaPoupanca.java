package br.ufc.quixada.cc.model;

public class ContaPoupanca extends ContaBancaria implements Imprimivel{
	private double limite;
	public ContaPoupanca(int numConta, double saldo, double limite) {
		super(numConta, saldo);
		this.limite = limite;
		// TODO Auto-generated constructor stub
	}

	
	public double getLimite() {
		return limite;
	}


	public void setLimite(double limite) {
		this.limite = limite;
	}


	@Override
	public double sacar(double valor) {
		if(this.saldo+this.limite >= valor) {
			this.saldo = saldo  - valor;			

			return valor;
		}
		
		return 0;	
	}

	@Override
	public double depositar(double valor) {
		this.saldo = (saldo + valor);
		return valor;
	}

	@Override
	public void mostrarDados() {
		System.out.println("Numero da conta poupanca: " + getNumConta());
		System.out.println("saldo: " + getSaldo());
		System.out.println("limite: " + this.limite);		
	}
	
	public void transferir(double valor, ContaBancaria p) {
		sacar(valor);
		p.saldo = saldo + depositar(valor);
	}

}
