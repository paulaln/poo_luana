package br.ufc.quixada.cc.model;

public class ContaCorrente extends ContaBancaria implements Imprimivel{
	private double taxaDeOperacao; 
	

	public ContaCorrente(int numConta, double saldo, double taxaDeOperacao) {
		super(numConta, saldo);
		this.taxaDeOperacao = taxaDeOperacao;
		// TODO Auto-generated constructor stub
	}
	

	public double getTaxaDeOperacao() {
		return taxaDeOperacao;
	}


	public void setTaxaDeOperacao(double taxaDeOperacao) {
		this.taxaDeOperacao = taxaDeOperacao;
	}

	

	@Override
	public double depositar(double valor) {
		this.saldo = (saldo + valor) - taxaDeOperacao;
		return valor;
	}

	@Override
	public void mostrarDados() {
		System.out.println("Numero da conta corrente: " + getNumConta());
		System.out.println("saldo: " + getSaldo());
		System.out.println("taxa de operacao: " + taxaDeOperacao);
		
	}


	@Override
	public double sacar(double valor) {
		double aux = (saldo - valor) - taxaDeOperacao;
		if(aux > saldo) {
			return 0;
		}
		this.saldo = (saldo - valor) - taxaDeOperacao;
		return valor;
	}
	public void transferir(double valor, ContaBancaria p) {
		sacar(valor);
		p.saldo = saldo + depositar(valor);
	}




	

}
