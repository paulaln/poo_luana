package br.ufc.quixada.cc.model;

public abstract class ContaBancaria {
	private int numConta;
	protected double saldo;
	
	public ContaBancaria() {
		
	}
	public ContaBancaria(int numConta, double saldo) {
		this.numConta = numConta;
		this.saldo = saldo;
	}
	
	
	public int getNumConta() {
		return numConta;
	}


	public void setNumConta(int numConta) {
		this.numConta = numConta;
	}


	public double getSaldo() {
		return saldo;
	}


	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}


	public abstract double sacar(double valor);
	public abstract double depositar(double valor);
	public void transferir(double valor, int numConta) {
	}


	public void mostrarDados() {
		// TODO Auto-generated method stub
		
	}
	



	

}
