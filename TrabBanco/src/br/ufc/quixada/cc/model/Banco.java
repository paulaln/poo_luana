package br.ufc.quixada.cc.model;
import java.util.*;

public class Banco implements Imprimivel{
	public static Scanner in = new Scanner(System.in);
	public Banco(){
		
	}
	List<ContaBancaria> contas = new ArrayList<>();
	
	public void inserir(ContaBancaria p) {
		contas.add(p);
                    
	}
	
	public void remover(ContaBancaria p) {
		contas.remove(p);
	}
	
	public ContaBancaria procurar(int numConta){
		for(ContaBancaria p : contas) {
			if(numConta == p.getNumConta()) {
				return p;
			}
			
		}
		return null;
		
	}

	@Override
	public void mostrarDados() {
		for(ContaBancaria conta : contas){
            if(conta instanceof ContaCorrente){  
            	((ContaCorrente) conta).mostrarDados();  
            }else {
            	((ContaPoupanca) conta).mostrarDados(); 
            }
		}
		
	}
	
	//metodo do menu
	public boolean procurarConta(int numConta) {
		for(ContaBancaria p : contas) {
			if(numConta == p.getNumConta()) {
				return true;
			}
		}
		return false;
	}
	
}
