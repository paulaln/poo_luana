package br.ufc.quixada.cc.exec;

import java.time.LocalDate;
import java.util.Scanner;
import br.ufc.quixada.cc.model.*;

public class Principal {

    public static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {
        // TODO Auto-generated method stub


        Agenda agenda = new Agenda();

        LocalDate dataNasc = LocalDate.of(2010, 10, 11);
        Pessoa rita = new PessoaFisica("Rita", "ritadek12@gmail.com", "sebastiao", 123, dataNasc, "casada");
        Pessoa paulo = new PessoaFisica("Paulo", "paulofab@gmail.com", "sebastiao", 786, dataNasc, "casado");

        Pessoa luana = new PessoaJuridica("Paula", "paulaln09@gmail.com", "oscar", 986, 3547, "aabb");
        Pessoa jaci = new PessoaJuridica("Jaci", "jaci@gmail.com", "rua onze", 1567, 3547, "aabb");


        String continuar;
        do{
            agenda.menuPrincipal();
            int menuPrincipal = in.nextInt();
            //adicionando contato

            if (menuPrincipal == 1) {
                agenda.adicionar(rita);
                agenda.adicionar(paulo);

                agenda.adicionar(luana);
                agenda.adicionar(jaci);

            }//removendo contato
            else if (menuPrincipal == 2) {
                System.out.print("Nome do contato: ");
                String nome = in.next();
                agenda.remover(nome);

            }//pesquisando
            else if (menuPrincipal == 3) {
                agenda.menuPesquisa();
                int menuPesquisa = in.nextInt();
                if (menuPesquisa == 1) {
                    String nome = in.next();
                    agenda.pesquisar(nome);

                } else if (menuPesquisa == 2) {
                    int cpf = in.nextInt();
                    agenda.pesquisaCpf(cpf);

                } else if (menuPesquisa == 3) {
                    int cnpj = in.nextInt();
                    agenda.pesquisaCnpj(cnpj);

                } else if (menuPesquisa == 4) {
                    agenda.listarTodos();
                }

            }
            System.out.print("Deseja continuar(s/n): ");
            continuar = in.next();

        }while( continuar.equalsIgnoreCase("s"));

    }
}