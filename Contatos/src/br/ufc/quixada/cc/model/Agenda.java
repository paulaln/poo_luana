package br.ufc.quixada.cc.model;
import java.util.ArrayList;
public class Agenda {
	
		ArrayList<Pessoa> listaDePessoas = new ArrayList<>();
				
			
		public void remover(String nome) {
			for(Pessoa i: listaDePessoas) {
				if(i.getNome().equals(nome)) {
					listaDePessoas.remove(i);
					System.out.println("Contato removido!");
				}else {
					System.out.println("Contato inexistente");
				}
			}
		}
		public void adicionar(Pessoa n) {
			if(listaDePessoas.add(n)) {
				System.out.println("Contato adicionado");
			}else {
				System.out.println("Erro ao cadastrar");
			}
		}
		
		//pesquisar contatos
		public void pesquisar(String nome) {
			for(Pessoa i: listaDePessoas) {
					if(i.getNome().equals(nome)) {
						System.out.println(i);
					}
				}
		}
				
		public void pesquisaCpf(int cpf) {
			for(Pessoa i: listaDePessoas) {
				if(i instanceof PessoaFisica) {
					PessoaFisica pf = (PessoaFisica)i;
					if(pf.getCpf() == cpf) {
						System.out.println((PessoaFisica) i);
					}
				}
			}
				
		}
		
		public void pesquisaCnpj(int cnpj) {
			for(Pessoa i: listaDePessoas) {
				if(i instanceof PessoaJuridica) {
					if(((PessoaJuridica)i).getCnpj() == (cnpj)) {
						System.out.println((PessoaJuridica) i);
					}
				}
			}
		}
			
		public void listarTodos() {
			for(Pessoa i: listaDePessoas) {
				System.out.println(i);
			}
			
		}
		
		public void menuPesquisa() {
			System.out.println("Menu de pesquisa");
			System.out.println("1 - Pesquisar por nome");
			System.out.println("2 - Pesquisar por cpf");
			System.out.println("3 - Pesquisar por cnpj");
		
		}
	
		public void menuPrincipal() {
			System.out.println("Agenda de contatos");
			System.out.println("1 - adicionar contato");
			System.out.println("2 - remover por nome");
			System.out.println("3 - pesquisar");
			System.out.println("4 - listar");
			System.out.println("5 - Ordenacao CPF");
			System.out.println("6 - Ordenacao CNPJ");
			System.out.println("7 - Lista ordenada ");
		}
		
		public void ordenaCpf() {
			for(int i = 0; i < listaDePessoas.size(); i++) {
				int menor = i;
				for(int j = i+1; j < listaDePessoas.size();j++) {
					if(listaDePessoas.get(j) instanceof PessoaFisica & listaDePessoas.get(menor) instanceof PessoaFisica) {
						if(((PessoaFisica) listaDePessoas.get(j)).getCpf() < ((PessoaFisica) listaDePessoas.get(menor)).getCpf()) {
							menor = j;
						}
					}
				}
				Pessoa aux = listaDePessoas.get(i);
				listaDePessoas.set(i, listaDePessoas.get(menor));
				listaDePessoas.set(menor, aux);
			}
			
		}
		public void ordenaCnpj() {
			for(int i = 0; i < listaDePessoas.size(); i++) {
				int menor = i;
				for(int j = i+1; j < listaDePessoas.size();j++) {
					if(listaDePessoas.get(j) instanceof PessoaJuridica & listaDePessoas.get(menor) instanceof PessoaJuridica) {
						if(((PessoaJuridica) listaDePessoas.get(j)).getCnpj() < ((PessoaJuridica) listaDePessoas.get(menor)).getCnpj()) {
							menor = j;
						}
					}
				}
				Pessoa aux = listaDePessoas.get(i);
				listaDePessoas.set(i, listaDePessoas.get(menor));
				listaDePessoas.set(menor, aux);
			}
			
		}
	
}


