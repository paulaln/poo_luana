package br.ufc.quixada.cc.model;

import java.time.LocalDate;

public class PessoaFisica extends Pessoa{
	private int cpf;
	private LocalDate nasc;
	private String estadoCivil;
	
	public PessoaFisica(String nome, String endereco, String email, int cpf, LocalDate nasc, String estadoCivil) {
		super(nome,endereco, email);
			this.cpf = cpf;
			this.nasc = nasc;
			this.estadoCivil = estadoCivil;
		
	}

	public int getCpf() {
		return cpf;
	}

	public void setCpf(int cpf) {
		this.cpf = cpf;
	}

	public LocalDate getNasc() {
		return nasc;
	}

	public void setNasc(LocalDate nasc) {
		this.nasc = nasc;
	}


	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	@Override
	public String toString() {
		return "Nome = " + nome + '\n' + "Endereco = " + endereco + '\n' + "Email = " + email + '\n'+ 
				"Cpf = " + cpf + ", \nData de nascimento = " + nasc + '\n' + "Estado civil = " + estadoCivil;
	}
	

	

	
}
