package br.ufc.quixada.cc.model;


public class PessoaJuridica extends Pessoa{
	private int cnpj;
	private int inscricaoEstadual;
	private String razaoSocial;
	
	public PessoaJuridica(String nome, String endereco, String email, int cnpj, int inscricaoEstadual, String razaoSocial) {
			super(nome, endereco, email);
			this.cnpj = cnpj;
			this.inscricaoEstadual = inscricaoEstadual;
			this.razaoSocial = razaoSocial;
		
	}

	public int getCnpj() {
		return cnpj;
	}

	public void setCnpj(int cnpj) {
		this.cnpj = cnpj;
	}

	public int getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(int inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	@Override
	public String toString() {
		return "Nome = " + nome + '\n' + "Endereco = " + endereco + '\n' + "Email = " + email + '\n' +
				"CNPJ = " + cnpj + "Inscricao estadual = " + inscricaoEstadual + '\n' + "Razao social = " + razaoSocial;
	}
	
	
	

}
