package br.ufc.quixada.model;

public class Corrida {
	
	private String partida;
	private String destino;
	private float precoKM;
	private float precoCorrida;
	public int distancia = 5;
	
	public Corrida() {
		
	}
	public Corrida(String partida, String destino, float precoKM) {
		this.partida = partida;
		this.destino = destino;
		this.precoKM = precoKM;
		this.precoCorrida = 0;
	}

	public String getPartida() {
		return partida;
	}

	public void setPartida(String partida) {
		this.partida = partida;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public float getPrecoKM() {
		return precoKM;
	}

	public void setPrecoKM(float precoKM) {
		this.precoKM = precoKM;
	}

	public float getPrecoCorrida() {
		return precoCorrida;
	}

	public void setPrecoCorrida(float precoCorrida) {
		this.precoCorrida = precoCorrida;
	}
	
	public float calcularValorCorrida(int distancia, float precoKM) {
		float valor = distancia * precoKM;
		return valor;
	}
	
	public String toString() {
		   String corrida = "";
		          corrida = "Ponto de partida: " + this.partida + '\n' +
		        		  	"Destino: "          + this.destino + '\n' +
				            "Pre�o por KM: "     + this.precoKM + '\n' +
				            "Pre�o da corrida"   + calcularValorCorrida(distancia, precoKM);
		          return corrida;
	}

}
