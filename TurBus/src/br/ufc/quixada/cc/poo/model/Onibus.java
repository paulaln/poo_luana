package br.ufc.quixada.cc.poo.model;

import java.util.ArrayList;
import java.util.List;

import br.ufc.quixada.cc.poo.interfaces.Imprimivel;

public class Onibus implements Imprimivel{
	private int codOnibus;
	private Motorista motorista;
	private List<Passagem> passagens;
	private List<Cliente> passageiros;
	
	public Onibus() {
		this.passagens = new ArrayList<>();
		this.passageiros = new ArrayList<>();
	}
	
	public Onibus(int codOnibus, Motorista motorista, List<Passagem> passagens, List<Cliente> passageiros) {
		this.codOnibus = codOnibus;
		this.motorista = motorista;
		this.passagens = passagens;
		this.passageiros = passageiros;
		
	}

	public int getCodOnibus() {
		return codOnibus;
	}

	public void setCodOnibus(int codOnibus) {
		this.codOnibus = codOnibus;
	}

	public Motorista getMotorista() {
		return motorista;
	}

	public void setMotorista(Motorista motorista) {
		this.motorista = motorista;
	}

	public List<Passagem> getPassagens() {
		return passagens;
	}

	public void setPassagens(List<Passagem> passagens) {
		this.passagens = passagens;
	}

	public List<Cliente> getPassageiros() {
		return passageiros;
	}

	public void setPassageiros(List<Cliente> passageiros) {
		this.passageiros = passageiros;
	}
	
	public void adicionarPassagemOnibus(Passagem p) {
		if(passagens.size() >= 63) {
			System.out.println("Nao ha passagem disponivel!");
		}else {
			passagens.add(p);
		}
	}
	
	@Override
	public void mostrarPassagens() {
		for(Passagem p : passagens) {
				System.out.println(p.toString());
			}
		
	}
	
	@Override
	public String toString() {
		return "Onibus [codOnibus=" + this.codOnibus + ", motorista=" + this.motorista + ", passagens=" + this.passagens
				+ ", passageiros=" + this.passageiros + "]";
	}

	
	
	
	
}
