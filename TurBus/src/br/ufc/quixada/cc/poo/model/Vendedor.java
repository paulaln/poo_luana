package br.ufc.quixada.cc.poo.model;

public class Vendedor extends Funcionario{
	private int cargaHoraria;
	
	public Vendedor() {
		
	}
	
	public Vendedor(String nome, String cpf, String matricula, float salario, int cargaHoraria) {
		super(nome, cpf, matricula, salario);
		this.cargaHoraria = cargaHoraria;
	}

	public int getCargaHoraria() {
		return cargaHoraria;
	}

	public void setCargaHoraria(int cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	
	public void darBonificacao() {
		super.setSalario(super.getSalario() + 3);
	}
	
	public void realizarVenda(Onibus o, Passagem p) {
		o.adicionarPassagemOnibus(p);
		darBonificacao();
	}
	
	@Override
	public String toString() {
		return super.toString() + "\n**Vendedor**\nCarga horaria = " + this.cargaHoraria;
	}
	
	

}
