package br.ufc.quixada.cc.poo.model;

import java.time.LocalDate;

public class ClientePessoaJuridica extends Cliente{
	private String cnpj;
	private LocalDate dataAbetura;
	
	public ClientePessoaJuridica() {
		
	}
	
	public ClientePessoaJuridica(String nome, String endereco, String cnpj, LocalDate dataAbertura) {
		super(nome, endereco);
		this.cnpj = cnpj;
		this.dataAbetura = dataAbertura;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public LocalDate getDataAbetura() {
		return dataAbetura;
	}

	public void setDataAbetura(LocalDate dataAbetura) {
		this.dataAbetura = dataAbetura;
	}

	@Override
	public String toString() {
		return super.toString() + "\nCliente pessoa juridica\nCNPJ = " + cnpj 
				+ ", data de abetura = " + dataAbetura;
	}
	
	

}
