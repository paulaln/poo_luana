package br.ufc.quixada.cc.poo.model;

public class Motorista extends Funcionario{
	
	private String cnh;
	
	public Motorista() {
		
	}
	
	public Motorista(String nome, String cpf, String matricula, float salario, String cnh) {
		super(nome, cpf, matricula, salario);
		this.cnh = cnh;
		
	}
	
	

	public String getCnh() {
		return cnh;
	}

	public void setCnh(String cnh) {
		this.cnh = cnh;
	}

	@Override
	public void darBonificacao() {
		float valor = ((super.getSalario() * 5)/100);
		super.setSalario(super.getSalario() + valor);
		
	}
	
	public void realizarViagem() {
		darBonificacao();
	}
	
	@Override
	public String toString() {
		return super.toString() + "\nMotorista\nCNH = " + cnh;
	}
	

}
