package br.ufc.quixada.cc.poo.model;

public class ServicosGerais extends Funcionario{
	private int tempoServico;
	
	public ServicosGerais() {
		
	}
	
	public ServicosGerais(String nome, String cpf, String matricula, float salario, int tempoServico) {
		super(nome, cpf, matricula, salario);
		this.tempoServico = tempoServico;
	}

	public int getTempoServico() {
		return tempoServico;
	}

	public void setTempoServico(int tempoServico) {
		this.tempoServico = tempoServico;
	}

	@Override
	public void darBonificacao() {
		super.setSalario(super.getSalario() + 3);
		
	}
	
	public void limpar() {
		darBonificacao();
	}
	

	@Override
	public String toString() {
		return super.toString() + "\nServicos Gerais\nTempo de servico = " + this.tempoServico;
	}
	
	
	
	
	

}
