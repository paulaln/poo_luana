package br.ufc.quixada.cc.poo.model;

public abstract class Funcionario extends Pessoa{
	private String cpf;
	private String matricula;
	private float salario;
	
	public Funcionario() {
		
	}
	
	public Funcionario(String nome, String cpf, String matricula, float salario) {
		super(nome);
		this.cpf = cpf;
		this.matricula = matricula;
		this.salario = salario;
		
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public float getSalario() {
		return salario;
	}

	public void setSalario(float salario) {
		this.salario = salario;
	}
	
	public abstract void darBonificacao();
	
	@Override
	public String toString() {
		return super.toString() + "\nDados do funcionario\n"
				+ "CPF = " + this.cpf + ", matricula=" + this.matricula + ", salario=" + this.salario + "]";
	}
	
	
}
