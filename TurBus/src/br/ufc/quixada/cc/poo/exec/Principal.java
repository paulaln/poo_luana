package br.ufc.quixada.cc.poo.exec;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


import br.ufc.quixada.cc.poo.model.*;

public class Principal {
	static List<Onibus> onibus = new ArrayList<>();
	static List<Pessoa> pessoa = new ArrayList<>();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Pessoa motorista = new Motorista("Joao", "444", "333",1200.3f, "AB");
		pessoa.add(motorista);
		
		int menu = 0, opcao = 0;
		
		/*** Menu **");
		("1 - Cadastrar onibus");
		("2 - Cadastrar cliente");
		("3 - Cadastrar funconario");
		("4 - Cadastrar passagem");
		("5 - Gerar + 1 - passagens, vendedor, cliente, onibus e motorista");*/
		
		menu = 1;
		switch(menu){
	    case 1:
	    	String cpf = "444";
	    	Motorista m = null;
	    	
	    	for(Pessoa p : pessoa) {
				if(p instanceof Motorista) {
					if(((Motorista)p).getCpf().equals(cpf)) {
						m = ((Motorista)p);
					}	
				}
			}
	    	Onibus onibus = new Onibus();
			onibus.setCodOnibus(347);
			onibus.setMotorista(m);
			break;
		
	    case 2:
	    	/*1 - pessoa f�sica; 2 - pessoa juridica*/
	    	opcao = 1;
	    	if(opcao == 1) {
	    		LocalDate d1 = LocalDate.of(1999, 8, 29);
	    		Pessoa c1 = new ClientePessoaFisica("Paula Luana", "Oscar Barbosa", "0000", d1);
	    		pessoa.add(c1);
	    		
	    	}else if(opcao == 2) {
	    		LocalDate dataAbertura = LocalDate.of(2018, 11, 30);
	    		Pessoa c2 = new ClientePessoaFisica("Paula Luana", "Oscar Barbosa", "12345", dataAbertura);
	    		pessoa.add(c2);
	    	}
	    	else {
	    		System.out.println("Valor invalido");
	    	}
	    	break;
	    	
	    case 3:
	    	/*1 - vendedor; 2 - servicos gerais; 3 - motorista*/
	    	opcao = 1;
	    	if(opcao == 1) {
	    		Pessoa f1 = new Vendedor("Ana", "123", "456", 800, 8);
	    		pessoa.add(f1);
	    		
	    	}else if(opcao == 2) {
	    		Pessoa f2 = new ServicosGerais("Roberto", "890", "789", 600, 6);
	    		pessoa.add(f2);
	    	}
	    	else if(opcao == 3) {
	    		Pessoa f3 = new Motorista("Joao", "444", "333",1200.3f, "AB");
	    		pessoa.add(f3);
	    		
	    	}else {
	    		System.out.println("Valor invalido");
	    	}
	    	break;
	    
	    case 4:
	    	String cpfCliente = "123", cpfFuncionario = "456";
	    	ClientePessoaFisica c = null;
	    	Vendedor vend1 = null;
	    	for(Pessoa p : pessoa) {
					if(((ClientePessoaFisica)p).getCpf().equals(cpfCliente)) {
						c = ((ClientePessoaFisica)p);
					}
					
					if(((Vendedor)p).getCpf().equals(cpfFuncionario)) {
						vend1 = ((Vendedor)p);
					}
			}
	    	
	    	LocalDate dataPartida = LocalDate.of(2018, 11, 30);
	    	Passagem p1 = new Passagem(123, c, vend1, dataPartida, 18, 6, 25.5f);
	    	
	    	Motorista mt = new Motorista("Joao", "444", "333",1200.3f, "AB");
	    	Onibus o = new Onibus();
			o.setCodOnibus(347);
			o.setMotorista(mt);
			o.adicionarPassagemOnibus(p1);
			
	    	vend1.realizarVenda(o, p1);
	    	break;
	    case 5:
	    	LocalDate d2 = LocalDate.of(1979, 7, 9);
	    	LocalDate dataPartida2 = LocalDate.of(2018, 11, 30);
	    	Cliente cliente2 = new ClientePessoaFisica("Rita de Cassia", "Quartzo", "0000", d2);
	    	Vendedor vendedor2 = new Vendedor("Paulo", "452874", "4632", 800, 8);
	    	Motorista motorista2 = new Motorista("Jose", "292", "3248",1000.3f, "AB");
	    	Passagem p2 = new Passagem(231, cliente2, vendedor2, dataPartida2, 18, 7, 25.5f);
	    	
	    	Onibus onibus2 = new Onibus();
			onibus2.setCodOnibus(472);
			onibus2.setMotorista(motorista2);
			onibus2.adicionarPassagemOnibus(p2);
			
	    	break;
	    default: System.out.println("Opcao incorreta!");
	    	
		}
		
		

	}

}
