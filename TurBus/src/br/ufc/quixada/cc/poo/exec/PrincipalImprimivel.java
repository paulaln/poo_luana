package br.ufc.quixada.cc.poo.exec;

import java.time.LocalDate;
import br.ufc.quixada.cc.poo.interfaces.*;
import br.ufc.quixada.cc.poo.model.*;

public class PrincipalImprimivel {
	

	
	public static void main(String[] args) {
		
		LocalDate d1 = LocalDate.of(1999, 8, 29);
		LocalDate d2 = LocalDate.of(1979, 7, 9);
		LocalDate d3 = LocalDate.of(2000, 12, 25);
		LocalDate dataPartida = LocalDate.of(2018, 11, 30);
		
		Cliente c1 = new ClientePessoaFisica("Paula Luana", "Oscar Barbosa", "0000", d1);
		Cliente c2 = new ClientePessoaFisica("Rita de Cassia", "Quartzo", "0000", d2);
		Cliente c3 = new ClientePessoaFisica("Paulo Joel", "Quartzo", "0000", d3);
		
		Funcionario v1 = new Vendedor("Ana", "123", "456", 800, 8);
		Funcionario v2 = new Vendedor("Roberto", "890", "789", 600, 6);
		Funcionario v3 = new Vendedor("Francisco", "789", "012", 1000, 9);
		
		Passagem p1 = new Passagem(123, c1, v1, dataPartida, 18, 6, 25.5f);
		Passagem p2 = new Passagem(231, c2, v2, dataPartida, 18, 7, 25.5f);
		Passagem p3 = new Passagem(321, c3, v3, dataPartida, 18, 8, 25.5f);
		
		Motorista motorista = new Motorista("Joao", "444", "333",1200.3f, "AB");
		
		Onibus onibus = new Onibus();
		onibus.setCodOnibus(347);
		onibus.setMotorista(motorista);
		onibus.adicionarPassagemOnibus(p1);
		onibus.adicionarPassagemOnibus(p2);
		onibus.adicionarPassagemOnibus(p3);
		
		((Imprimivel)onibus).mostrarPassagens();
		
		
		
		
		
		
	}
	

}
